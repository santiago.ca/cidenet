package cidenet;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.ComponentScan;

/**
 * Excecute this class as a java application for initiate 
 * the page (port is 8080)
 * @author Santiago
 *
 */
@SpringBootApplication
@ComponentScan({"cidenet"})
public class CidenetApplication {

	public static void main(String[] args) {
		SpringApplication.run(CidenetApplication.class, args);
	}

}
