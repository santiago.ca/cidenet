package cidenet.api.controller;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

import cidenet.entity.Area;
import cidenet.repository.AreaRepository;

/**
 * This class manage http request
 * for Area
 * 
 * @author Santiago
 *
 */
@RestController
public class AreaApiController {

	@Autowired
	AreaRepository repository;
	
	@GetMapping("/areas")
	public List<Area> retrieveAllAreas(){
		List<Area> allAreas = new ArrayList<Area>();
		for(Area e : repository.findAll()) {
			allAreas.add(e);
		}
		return allAreas;
	}
}
