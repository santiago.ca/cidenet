package cidenet.api.controller;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

import cidenet.entity.Country;
import cidenet.repository.CountryRepository;

/**
 * This class manage http request
 * for Country
 * 
 * @author Santiago
 *
 */
@RestController
public class CountryApiController {
	
	@Autowired
	CountryRepository countryRepository;
	
	@GetMapping(value="/countries")
	public List<Country> retrieveCountries(){
		List<Country> allCountries = new ArrayList<Country>();
		for(Country e : countryRepository.findAll()) {
			allCountries.add(e);
		}
		return allCountries;
	}

}
