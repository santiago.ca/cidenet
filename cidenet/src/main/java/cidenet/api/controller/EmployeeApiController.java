package cidenet.api.controller;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.validation.FieldError;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

import cidenet.entity.Employee;
import cidenet.repository.EmployeeRepository;
import cidenet.repository.StatusRepository;

/**
 * This class manage http request
 * for Employee API.
 * 
 * Allow retrieve all employees,
 * save an employee,
 * edit an employee,
 * delete an employee and others
 * 
 * @author Santiago
 *
 */
@RestController
public class EmployeeApiController {

	/**
	 * We use "autowired" annotation to bring
	 * the interfaz which allows us to
	 * do jpa transactions
	 */
	@Autowired
	EmployeeRepository repository;
	@Autowired
	StatusRepository sRepository;

	/**
	 * Repository give us all the employees
	 * @return Json with a list of employees (Based on Employee class)
	 */
	@GetMapping(value = "/employees")
	public List<Employee> retrieveAllEmployees(){
		List<Employee> allEmployees = new ArrayList<Employee>();
		for(Employee e : repository.findAll()) {
			allEmployees.add(e);
		}
		return allEmployees;
	}

	/**
	 * Search an employee by id
	 * @param id Employee identifier (Id field in data base)
	 * @return Employee with the id asked
	 */
	@GetMapping(value = "employee/{id}")
	public Employee retrieveEmployee(@PathVariable Long id) {
		return repository.findById(id).get();
	}

	/**
	 * This method recives a valid employee and generate the info 
	 * which canth be changed by the user
	 * @param newEmployee Employee to save (is validated through hibernate)
	 * @return employee created (error message in case of validation fails)
	 */
	@PostMapping(value="/addEmployee")
	public Employee saveEmployee(@Valid @RequestBody Employee newEmployee) {
		if(newEmployee.getId()!=-1) newEmployee.setEmail(repository.findById(newEmployee.getId()).get().getEmail());
		if(newEmployee.getEmail()==null) {
			//Generate a unique email
			int actN=0;
			while(repository.findByEmail(newEmployee.generateEmail(actN))!=null) {
				actN++;
			}
			newEmployee.setEmail(newEmployee.generateEmail(actN));
		}
		//Status
		newEmployee.setStatus(sRepository.findByName("Activo"));
		//Date of entry (actual date)
		newEmployee.setDateOfEntry(new Timestamp(System.currentTimeMillis()));
		return repository.save(newEmployee);
	}

	/**
	 * Delete employee
	 * @param id Identifier of the employee to delete (Id field in database)
	 */
	@DeleteMapping(value="/deleteEmployee/{id}")
	public void deleteEmployee(@PathVariable Long id) {
		repository.deleteById(id);
	}

	/**
	 * Class to handle possible errors
	 */
	@ResponseStatus(code = HttpStatus.BAD_REQUEST)
	@ExceptionHandler(MethodArgumentNotValidException.class)
	public Map<String, String> handleValidationExceptions(
			MethodArgumentNotValidException ex) {
		Map<String, String> errors = new HashMap<>();
		ex.getBindingResult().getAllErrors().forEach((error) -> {
			String fieldName = ((FieldError) error).getField();
			String errorMessage = error.getDefaultMessage();
			errors.put(fieldName, errorMessage);
		});
		return errors;
	}
}
