package cidenet.api.controller;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

import cidenet.entity.TypeOfId;
import cidenet.repository.TypeOfIdRepository;

/**
 * This class manage http request
 * for Type of Id
 * 
 * @author Santiago
 *
 */
@RestController
public class TypeOfIdApiController {

	@Autowired
	TypeOfIdRepository repository;
	
	@GetMapping("/typesOfId")
	public List<TypeOfId> retrieveAllAreas(){
		List<TypeOfId> allTypes = new ArrayList<TypeOfId>();
		for(TypeOfId e : repository.findAll()) {
			allTypes.add(e);
		}
		return allTypes;
	}
}
