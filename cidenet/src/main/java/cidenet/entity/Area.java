package cidenet.entity;

import javax.persistence.Entity;
import javax.persistence.Id;

/**
 * This class is a representation of
 * the table "AREA"
 * 
 * @author Santiago
 *
 */
@Entity(name = "AREA")
public class Area {
	
	@Id
	private int id;

	private String name;

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public Area(String name) {
		super();
		this.name = name;
	}

	public Area() {
		super();
	}

}
