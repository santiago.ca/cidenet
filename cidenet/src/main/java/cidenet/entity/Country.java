package cidenet.entity;

import javax.persistence.Entity;
import javax.persistence.Id;

/**
 * This class is a representation of
 * the table "Country"
 * 
 * @author Santiago
 *
 */
@Entity(name = "COUNTRY")
public class Country {
	
	@Id
	private int id;
	
	private String name;
	private String domain;
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getDomain() {
		return domain;
	}
	public void setDomain(String domain) {
		this.domain = domain;
	}
	public Country(String name, String domain) {
		super();
		this.name = name;
		this.domain = domain;
	}
	public Country() {
		super();
	}
	@Override
	public String toString() {
		return "Country [id=" + id + ", name=" + name + ", domain=" + domain + "]";
	}

}
