package cidenet.entity;

import java.sql.Timestamp;
import java.text.SimpleDateFormat;
import java.util.Date;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.UniqueConstraint;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.Past;
import javax.validation.constraints.Pattern;

import org.hibernate.validator.constraints.Length;
import org.springframework.beans.propertyeditors.CustomDateEditor;
import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.web.bind.WebDataBinder;
import org.springframework.web.bind.annotation.InitBinder;

/**
 * This class is a representation of
 * the table "EMPLOYEE"
 * 
 * it has several validations, like:
 * No employee name can be blank,
 * and each one has a maximum length,
 * This fields only accept capital letters,
 * The Date of Admission can't be a future date
 * or have more than a month old, etc.
 * 
 * Additional, we use annotations for 
 * representing the relations with other tables (@ManyToOne)
 * 
 * @author Santiago
 *
 */
@Entity
@Table
public class Employee {
	
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long id;
	
	@NotBlank(message = "El primer apellido es obligatorio")
	@Length(max=20, message = "La maxima longitud para el primer apellido es de 20 caracteres") 
	@Pattern(regexp = "[A-Z ]*", message = "Utiliza solamente letras mayusculas para el primer apellido")
	private String surname;
	
	@NotBlank(message = "El segundo apellido es obligatorio")
	@Length(max=20, message = "La maxima longitud para el segundo apellido es de 20 caracteres") 
	@Pattern(regexp = "[A-Z ]*", message = "Utiliza solamente letras mayusculas para el segundo apellido")
	private String secondSurname;
	
	@NotBlank(message = "El primer primer es obligatorio")
	@Length(max=20, message = "La maxima longitud para el primer nombre es de 20 caracteres") 
	@Pattern(regexp = "[A-Z]*", message = "Utiliza solamente letras mayusculas para el primer nombre")
	private String firstName;
	
	@Length(max=50, message = "La maxima longitud para otros nombres es de 50 caracteres") 
	@Pattern(regexp = "[A-Z ]*", message = "Utiliza solamente letras mayusculas para otros nombres")
	private String otherNames;
	
	@ManyToOne
	private Country jobCountry;
	@ManyToOne
	private TypeOfId typeOfId;
	
	@NotBlank(message = "El numero de identifiación es obligatorio")
	@Length(max=20, message = "La maxima longitud para el numero de identificación es de 20 caracteres")
	@Pattern(regexp = "[a-zA-Z0-9-]*", message = "Utiliza solo letras, numeros o '-' para el numero de identificación")
	private String idNumber;
	
	@Length(max=300)
	private String email;
	
	private Date dateOfAdmission;
	
	@ManyToOne
	private Area area;
	@ManyToOne
	private	Status status;
	
	private Timestamp dateOfEntry;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getSurname() {
		return surname;
	}

	public void setSurname(String surname) {
		this.surname = surname;
	}

	public String getSecondSurname() {
		return secondSurname;
	}

	public void setSecondSurname(String secondSurname) {
		this.secondSurname = secondSurname;
	}

	public String getFirstName() {
		return firstName;
	}

	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}

	public String getOtherNames() {
		return otherNames;
	}

	public void setOtherNames(String otherNames) {
		this.otherNames = otherNames;
	}

	public TypeOfId getTypeOfId() {
		return typeOfId;
	}

	public void setTypeOfId(TypeOfId typeOfId) {
		this.typeOfId = typeOfId;
	}

	public String getIdNumber() {
		return idNumber;
	}

	public void setIdNumber(String idNumber) {
		this.idNumber = idNumber;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public Date getDateOfAdmission() {
		return dateOfAdmission;
	}

	public void setDateOfAdmission(Date dateOfAdmission) {
		this.dateOfAdmission = dateOfAdmission;
	}

	public Timestamp getDateOfEntry() {
		return dateOfEntry;
	}

	public void setDateOfEntry(Timestamp dateOfEntry) {
		this.dateOfEntry = dateOfEntry;
	}

	public Area getArea() {
		return area;
	}

	public void setArea(Area area) {
		this.area = area;
	}

	public Status getStatus() {
		return status;
	}

	public void setStatus(Status status) {
		this.status = status;
	}

	public Employee() {
		super();
	}
	
	public Country getJobCountry() {
		return jobCountry;
	}

	public void setJobCountry(Country jobCountry) {
		this.jobCountry = jobCountry;
	}

	public Employee(
			@NotBlank(message = "Surname is mandatory") @Length(max = 20, message = "The maximum lenght for surname is 20 characters") @Pattern(regexp = "[A-Z ]*", message = "Use only capital letters for surname") String surname,
			@NotBlank(message = "Second surname is mandatory") @Length(max = 20, message = "The maximum lenght for second surname is 20 characters") @Pattern(regexp = "[A-Z ]*", message = "Use only capital letters for second surname") String secondSurname,
			@NotBlank(message = "First name is mandatory") @Length(max = 20, message = "The maximum lenght for first name is 20 characters") @Pattern(regexp = "[A-Z]*", message = "Use only capital letters for first name") String firstName,
			@Length(max = 50, message = "The maximum lenght for other names is 50 characters") @Pattern(regexp = "[A-Z ]*", message = "Use only capital letters for other names") String otherNames,
			Country jobCountry, TypeOfId typeOfId,
			@NotBlank(message = "Id Number is mandatory") @Length(max = 20, message = "The maximum length for id number is 20 characters") @Pattern(regexp = "[a-zA-Z0-9-]*", message = "Use only letters, numbers or '-' for id number") String idNumber,
			String email, @Past Date dateOfAdmission, Area area, Status status, Timestamp dateOfEntry) {
		super();
		this.surname = surname;
		this.secondSurname = secondSurname;
		this.firstName = firstName;
		this.otherNames = otherNames;
		this.jobCountry = jobCountry;
		this.typeOfId = typeOfId;
		this.idNumber = idNumber;
		this.email = email;
		this.dateOfAdmission = dateOfAdmission;
		this.area = area;
		this.status = status;
		this.dateOfEntry = dateOfEntry;
	}
	
	/**
	 * This method generates an email for the current user
	 * depending on the n
	 * @param n Number of email 
	 * @return email with structure firstname.surname.n@domain;
	 */
	public String generateEmail(int n) {
		return this.firstName.toLowerCase()+"."+this.surname.toLowerCase().replace(" ","")+(n!=0?"."+n:"")+"@"+this.jobCountry.getDomain();
	}

	@Override
	public String toString() {
		return "Employee [id=" + id + ", surname=" + surname + ", secondSurname=" + secondSurname + ", firstName="
				+ firstName + ", otherNames=" + otherNames + ", jobCountry=" + jobCountry + ", typeOfId=" + typeOfId
				+ ", idNumber=" + idNumber + ", email=" + email + ", dateOfAdmission=" + dateOfAdmission + ", area="
				+ area + ", status=" + status + ", dateOfEntry=" + dateOfEntry + "]";
	}

}
