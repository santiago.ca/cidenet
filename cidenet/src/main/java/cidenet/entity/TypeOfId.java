package cidenet.entity;

import javax.persistence.Entity;
import javax.persistence.Id;

/**
 * This class is a representation of
 * the table "TYPE_OF_ID"
 * 
 * @author Santiago
 *
 */
@Entity(name = "TYPE_OF_ID")
public class TypeOfId {
	
	@Id
	private int id;
	
	private String name;
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public TypeOfId(String name) {
		super();
		this.name = name;
	}
	public TypeOfId() {
		super();
	}
	
}
