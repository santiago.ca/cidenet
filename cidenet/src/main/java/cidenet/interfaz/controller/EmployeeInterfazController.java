package cidenet.interfaz.controller;

import org.springframework.data.domain.Pageable;
import java.sql.Timestamp;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.time.LocalDate;
import java.time.ZoneId;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.propertyeditors.CustomDateEditor;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.http.ResponseEntity;
import org.springframework.http.client.ClientHttpRequestInterceptor;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.util.CollectionUtils;
import org.springframework.validation.BindingResult;
import org.springframework.validation.ObjectError;
import org.springframework.web.bind.WebDataBinder;
import org.springframework.web.bind.annotation.InitBinder;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.client.RestTemplate;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.JsonMappingException;

import cidenet.entity.Area;
import cidenet.entity.Country;
import cidenet.entity.Employee;
import cidenet.entity.TypeOfId;
import cidenet.extra.EmployeeRestTemplateInterceptor;
import cidenet.repository.EmployeeRepository;
import cidenet.repository.StatusRepository;

/**
 * This controller manages
 * the visual part of the aplication
 *  
 * @author Santiago
 *
 */
@Controller
public class EmployeeInterfazController {
	
	/**
	 * This repositories help bring 
	 * data not mapped for API
	 */
	@Autowired
	EmployeeRepository empRepository;
	
	// This variable help us send http request to API
	RestTemplate restTemplate;
	
	/**
	 * This method loads the view for adding or
	 * editing a new employee
	 * we call the services for each selectable option (area, type of id, country)
	 * in order to obtain all data and let the user pick one. We put all these in the model
	 * @param model view model
	 * @param id employee id to edit (null if adding)
	 * @return view for editing the employee
	 */
	@RequestMapping(value={"/editEmployee"}, method = RequestMethod.GET)
	public String addEmployees(ModelMap model, @RequestParam Long id) {
		String url = "http://localhost:8080/";
		restTemplate = new RestTemplate();
		
		model.addAttribute("types", restTemplate.getForEntity(url+"typesOfId", TypeOfId[].class).getBody());
		model.addAttribute("countries",restTemplate.getForEntity(url+"countries", Country[].class).getBody());
		model.addAttribute("areas",restTemplate.getForEntity(url+"areas", Area[].class).getBody());
		if(id==-1) model.addAttribute("employee",new Employee("", "", "", "", null, null, "", "", new Date(System.currentTimeMillis()), null, null, null));
		else if(id!=-2) {
			model.addAttribute("employee", restTemplate.getForEntity(url+"employee/"+id, Employee.class).getBody());
		}
		return "employee";
	}

	/**
	 * This method sends the request for creating or editing
	 * a new user
	 * @param model view model 
	 * @param employee employee to update or add (has to be valid)
	 * @param result if succeed return to the principal view (show employees) if not 
	 * show errors
	 * @return
	 */
	@RequestMapping(value={"/editEmployee"}, method = RequestMethod.POST)
	public String sendEmployee(ModelMap model, @Valid Employee employee, BindingResult result) throws JsonMappingException, JsonProcessingException, ParseException {
		//Additional validations, like no repeat data 
		String isV = this.isValid(employee);
		if(result.hasErrors() || !isV.equals("")) {
			String allErrors = "";
			for(ObjectError e : result.getAllErrors()) allErrors+=e.getDefaultMessage()+"\\n";
			allErrors+=isV;
			//Error messages to show
			model.addAttribute("flag","error");
			model.addAttribute("message", allErrors);
			model.addAttribute("employee",employee);
			//Return to editing for fix the errors
			return addEmployees(model, (long)-2);
		}
		
		String url = "http://localhost:8080/addEmployee";
		restTemplate = new RestTemplate();
		List<ClientHttpRequestInterceptor> interceptors
		= restTemplate.getInterceptors();
		if (CollectionUtils.isEmpty(interceptors)) {
			interceptors = new ArrayList<>();
		}
		interceptors.add(new EmployeeRestTemplateInterceptor());
		restTemplate.setInterceptors(interceptors);
		//Add the new employee to database through the API
		restTemplate.postForEntity(url, employee, Employee.class);
		return "redirect:/";
		
	}
	
	/**
	 * Principal page, shows all employees
	 * @param model view model
	 * @param page actual page of employees (10 max in every page)
	 * @return the view with the employees for page
	 */
	@RequestMapping(value={"","/showEmployees"})
	public String showEmployees(ModelMap model, @RequestParam(defaultValue = "0") int page) {
		String url="http://localhost:8080/employees";
		Pageable empPage = PageRequest.of(page, 10);
		Page<Employee> allEmployees = empRepository.findAll(empPage);
		restTemplate = new RestTemplate();
		int totalEmp = (int) Math.ceil(restTemplate.getForEntity(url, Employee[].class).getBody().length*1.0/10*1.0);
		if(totalEmp==0) totalEmp=1;
		model.addAttribute("pages",totalEmp);
		model.addAttribute("employees", allEmployees.getContent());
		
		return "showEmployees";
	}
	
	//Delete employee and "reloads the page"
	@RequestMapping(value="/delEmp", method = RequestMethod.GET)
	public String deleteEmployee(ModelMap model, @RequestParam Long id) {
		String url="http://localhost:8080/deleteEmployee/"+id;
		restTemplate = new RestTemplate();
		restTemplate.delete(url);
		return showEmployees(model,0);
	}
	
	//Error page
	@RequestMapping(value="/error", method = RequestMethod.GET)
	public String showErrorPage() {
		return "error";
	}
	
	//This binder helps formating the date of admission field
	@InitBinder
	public void initBinder(WebDataBinder binder) {
		SimpleDateFormat dateFormat = new SimpleDateFormat("dd/MM/yyyy");
		binder.registerCustomEditor(Date.class, new CustomDateEditor(dateFormat, false));
	}
	
	//Extra validations for an employee
	private String isValid(Employee e) {
		String error="";
		if(e.getId()==-1 && empRepository.findByTypeOfIdAndIdNumber(e.getTypeOfId(), e.getIdNumber())!=null)
			error+="Ya existe un empleado con este tipo de identificación y numero de identifiación\\n";
		if(e.getDateOfAdmission().toInstant().atZone(ZoneId.systemDefault()).toLocalDate().isAfter(LocalDate.now()))
			error+="La fecha de ingreso debe ser maximo hoy\\n";
		else if(e.getDateOfAdmission().toInstant().atZone(ZoneId.systemDefault()).toLocalDate().isBefore(LocalDate.now().minusMonths(1)))
			error+="La fecha de ingreso no puede estar mas atras de hace un mes\\n";
		return error;
	}
}
