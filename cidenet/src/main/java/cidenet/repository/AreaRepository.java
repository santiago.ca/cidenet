package cidenet.repository;

import org.springframework.data.repository.CrudRepository;

import cidenet.entity.Area;

public interface AreaRepository extends CrudRepository<Area, Integer>{

}
