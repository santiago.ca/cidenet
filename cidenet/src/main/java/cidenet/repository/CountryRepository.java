package cidenet.repository;

import org.springframework.data.repository.CrudRepository;

import cidenet.entity.Country;

public interface CountryRepository extends CrudRepository<Country, Integer>{

}
