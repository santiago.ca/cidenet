package cidenet.repository;

import java.util.List;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.repository.PagingAndSortingRepository;

import cidenet.entity.Employee;
import cidenet.entity.TypeOfId;

public interface EmployeeRepository extends PagingAndSortingRepository<Employee, Long>{
	public Employee findByEmail(String email);
	public List<Employee> findByTypeOfId(TypeOfId type);
	public List<Employee> findByIdNumber(String number);
	public Employee findByTypeOfIdAndIdNumber(TypeOfId type, String number);
	public Page<Employee>findAll(Pageable pageable);
}
