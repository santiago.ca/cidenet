package cidenet.repository;

import org.springframework.data.repository.CrudRepository;

public interface StatusRepository extends CrudRepository<cidenet.entity.Status, Integer>{
	public cidenet.entity.Status findByName(String name);
}
