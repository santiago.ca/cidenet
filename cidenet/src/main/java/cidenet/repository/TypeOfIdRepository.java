package cidenet.repository;

import org.springframework.data.repository.CrudRepository;

import cidenet.entity.TypeOfId;

public interface TypeOfIdRepository extends CrudRepository<TypeOfId, Integer> {

}
