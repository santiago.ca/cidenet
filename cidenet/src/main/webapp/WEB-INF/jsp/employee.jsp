<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<html>
<head><meta charset="UTF-8"/></head>
<!-- CSS only -->
<link
	href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/css/bootstrap.min.css"
	rel="stylesheet"
	integrity="sha384-EVSTQN3/azprG1Anm3QDgpJLIm9Nao0Yz1ztcQTwFspd3yD65VohhpuuCOmLASjC"
	crossorigin="anonymous">
<!-- JavaScript Bundle with Popper -->
<script
	src="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/js/bootstrap.bundle.min.js"
	integrity="sha384-MrcW6ZMFYlzcLA8Nl+NtUVF0sA7MsXsP1UyJoMp4YLEuNSfAP+JcXn/tWtIaxVXM"
	crossorigin="anonymous"></script>
<script src="webjars/jquery/3.3.1-1/jquery.min.js"></script>
<script src="webjars/bootstrap-datepicker/1.7.1/js/bootstrap-datepicker.js"></script>
<script>
	$(function(){
		$('#dateOfAdmission').datepicker({
			format: 'dd/mm/yyyy'
		});
	})
	
</script>
<script>
	if('${flag}' === 'error'){
		alert('${message}',"Error");
	}
</script>
<title>Editar empleado</title>
<body>
	<h1 style="text-align:center" class="pull-center">Editar Empleado</h1>
	<br>
	<div>
	<form:form method="post" class="container col-3 pull-left" modelAttribute="employee">
		<div class="form-group">
			<form:label path="surname">Primer apellido</form:label> <form:input type="text" class="form-control"
				path="surname"/>
		</div>
		<div class="form-group">
			<form:label path="secondSurname">Segundo apellido</form:label> <form:input type="text" class="form-control"
				path="secondSurname"/>
		</div>
		<div class="form-group">
			<form:label path="firstName">Primer nombre</form:label> <form:input type="text" class="form-control"
				path="firstName"/>
		</div>
		<div class="form-group">
			<form:label path="otherNames">Otros nombres</form:label> <form:input type="text" class="form-control"
				path="otherNames"/>
		</div>
		<div class="form-group">
			<form:label path="jobCountry">País del empleo</form:label> <form:select class="form-select" path="jobCountry">
				<c:forEach var="country" items="${countries}">
					<form:option path="jobCountry" value="${country}">${country.getName()}</form:option>
				</c:forEach>
			</form:select>
		</div>
		<div class="form-group">
			<form:label path="typeOfId">Tipo de identificación</form:label> <form:select class="form-select" path="typeOfId">
				<c:forEach var="type" items="${types}">
					<form:option path="typeOfId" value="${type.getId()}">${type.getName()}</form:option>
				</c:forEach>
			</form:select>
		</div>
		<div class="form-group">
			<form:label path="idNumber">Numero de identifiación</form:label> <form:input type="text" class="form-control"
				path="idNumber"/>
		</div>
		<div class="form-group">
			<form:label path="dateOfAdmission">Fecha de ingreso</form:label>
			<form:input path="dateOfAdmission" class="form-control" required="required"/>
		</div>
		<div class="form-group">
			<form:label path="area">Area</form:label> <form:select class="form-select" path="area">
				<c:forEach var="area" items="${areas}">
					<form:option path="area" type="text" value="${area.getId()}">${area.getName()}</form:option>
				</c:forEach>
			</form:select>
		</div>
		<br> <input class="btn btn-success" type="submit" value="Enviar" /> <a href="/" class="btn btn-danger">Cancelar</a>
	</form:form>
	</div>
	
</body>
</html>