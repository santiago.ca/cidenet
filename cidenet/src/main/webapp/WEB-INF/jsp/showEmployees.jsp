<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>

<html>
<head><meta charset="UTF-8"/></head>
<!-- CSS only -->
<link
	href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/css/bootstrap.min.css"
	rel="stylesheet"
	integrity="sha384-EVSTQN3/azprG1Anm3QDgpJLIm9Nao0Yz1ztcQTwFspd3yD65VohhpuuCOmLASjC"
	crossorigin="anonymous">
<!-- JavaScript Bundle with Popper -->
<script
	src="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/js/bootstrap.bundle.min.js"
	integrity="sha384-MrcW6ZMFYlzcLA8Nl+NtUVF0sA7MsXsP1UyJoMp4YLEuNSfAP+JcXn/tWtIaxVXM"
	crossorigin="anonymous"></script>
<title>Empleados</title>
<body>

	<div>
		<h1>Todos los empleados</h1>
		<br>
		<table
			style="table-layout: auto; border-collapse: collapse; width: 100%;"
			class="table">
			<thead class="thead-dark">
				<tr>
					<th>Nombre</th>
					<th>País del empleo</th>
					<th>Tipo de identificación</th>
					<th>Numero de indentificación</th>
					<th>Email</th>
					<th>Fecha de ingreso</th>
					<th>Area</th>
					<th>Estado</th>
					<th>Fecha y hora de registro</th>
					<th>Editar</th>
					<th>Eliminar</th>
				</tr>
			</thead>
			<tbody>
				<c:forEach items="${employees}" var="employee">
					<tr>
						<td style="width: 100%">${employee.surname}
							${employee.secondSurname} ${employee.firstName}
							${employee.otherNames}</td>
						<td style="width: 100%">${employee.jobCountry.name}</td>
						<td style="width: 100%">${employee.typeOfId.name}</td>
						<td style="width: 100%">${employee.idNumber}</td>
						<td style="width: 100%">${employee.email}</td>
						<td style="width: 100%"><fmt:formatDate pattern="dd/MM/YYYY"
								value="${employee.dateOfAdmission}" /></td>
						<td style="width: 100%">${employee.area.name}</td>
						<td style="width: 100%">${employee.status.name}</td>
						<td style="width: 100%"><fmt:formatDate
								pattern="dd/MM/YYYY HH:mm:ss" value="${employee.dateOfEntry}" /></td>
						<td style="width: 100%"><a
							href="/editEmployee?id=${employee.id}" class="btn btn-success">Editar</a></td>
						<td style="width: 100%"><a href="/delEmp?id=${employee.id}"
							class="btn btn-danger">Eliminar</a></td>
					</tr>
				</c:forEach>
			</tbody>
		</table>
		<div>
			<ul class="pagination">
				<c:forEach begin="0" end="${pages-1}" var="page">
					<li class="page-item"><a class="page-link"
						href="/showEmployees?page=${page}">${page+1}</a></li>
				</c:forEach>
			</ul>
		</div>
		<div>
			<a href="/editEmployee?id=-1" class="btn btn-primary">Añadir
				empleado</a>
		</div>
	</div>

</body>

</html>